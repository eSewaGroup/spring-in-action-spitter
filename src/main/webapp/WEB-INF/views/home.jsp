<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<div>
    <h2>A global community of friends and strangers spitting out their
        inner-most and personal thoughts on the web for everyone else to
        see.</h2>
    <h3>Look at what these people are spitting right now...</h3>
    <s:url value="/spittles/delete" var="spittle_delete_url" />
    <ol class="spittle-list">
        <c:forEach var="spittle" items="${spittles}"> <!--<co id="cp_foreach_spittles"/>-->

            <s:url value="/spitters/{spitterName}"
                   var="spitter_url"> <!--<co id="cp_spitter_url"/>-->
                <s:param name="spitterName"
                         value="${spittle.spitter.username}"/>
            </s:url>

            <li><span class="spittleListImage">
        <img src="<s:url value="/resources/images/${spittle.spitter.id}.jpg"/>"
             width="48"
             height="48"
             onError=
                     "this.src='<s:url value="/resources/images"/>/spitter_avatar.png';"

                     <%--<s:url value="/resources/images/${spittle.spitter.id}.jpg"/>--%>
                     <%--width="48" height="48"--%>

        />
      </span>
                <span class="spittleListText">
        <a href="${spitter_url}">              <!--<co id="cp_spitter_properties"/>-->
          <c:out value="${spittle.spitter.username}"/></a>
          - <c:out value="${spittle.text}"/><br/>
         <small><fmt:formatDate value="${spittle.when}"
                                pattern="hh:mma MMM d, yyyy"/></small>
         <c:if test="${pageContext.request.userPrincipal.name == spittle.spitter.username}">
            <br>
             <s:url value="/spittles/{id}" var="spittle_update_url">
                 <s:param name="id" value="${spittle.id}"/>
             </s:url>
             <sf:form cssClass="form-button" action="${spittle_update_url}" method="POST">
									<input type="hidden" name="id" value="${spittle.id}">
									<input type="submit" value="Update">
             </sf:form>
             <sf:form cssClass="form-button" action="${spittle_delete_url}" method="POST">
									<input type="hidden" name="id" value="${spittle.id}">
									<input type="submit" value="Delete">
             </sf:form>
         </c:if>
      </span></li>
        </c:forEach>
    </ol>
</div>
