package com.habuma.spitter.service;

import com.habuma.spitter.domain.Spitter;
import com.habuma.spitter.domain.Spittle;

import java.util.List;

/**
 * Created by mohit on 5/2/18.
 */
public interface SpitterService {
    void saveSpitter(Spitter spitter);
    void updateSpitter(Spitter spitter);
    List<Spittle> getRecentSpittles(int count);

    Spitter getSpitter(String username);

    List<Spittle> getSpittlesForSpitter(String username);

    Spittle getSpittleById(int id);

    void saveSpittle(Spittle spittle);

    void deleteSpittle(int id);

    void updateSpittle(Spittle spittle);
}
