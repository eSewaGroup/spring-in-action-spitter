package com.habuma.spitter.service;

import com.habuma.spitter.domain.Spitter;
import com.habuma.spitter.domain.Spittle;
import com.habuma.spitter.persistence.SpitterDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import static java.lang.Math.min;
import static java.util.Collections.reverse;

/**
 * Created by mohit on 5/2/18.
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SpitterSerivceImpl implements SpitterService {
    private SpitterDao hibernateSpitterDao;

    @Autowired
    public SpitterSerivceImpl(SpitterDao hibernateSpitterDao) {
        this.hibernateSpitterDao = hibernateSpitterDao;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void saveSpitter(Spitter spitter) {
            hibernateSpitterDao.addSpitter(spitter);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateSpitter(Spitter spitter) {
        hibernateSpitterDao.saveSpitter(spitter);
    }

    @Override
    public List<Spittle> getRecentSpittles(int count) {
        List<Spittle> recentSpittles =
                hibernateSpitterDao.getRecentSpittle();

        reverse(recentSpittles);

        return recentSpittles.subList(0,
                min(49, recentSpittles.size()));
    }

    @Override
    public Spitter getSpitter(String username) {
        return hibernateSpitterDao.getSpitterByUsername(username);
    }

    @Override
    public List<Spittle> getSpittlesForSpitter(String username) {
        Spitter spitter = hibernateSpitterDao.getSpitterByUsername(username);
        return hibernateSpitterDao.getSpittlesForSpitter(spitter);
    }

    @Override
    public Spittle getSpittleById(int id) {
        return hibernateSpitterDao.getSpittleById(id);
    }

    @Override
    public void saveSpittle(Spittle spittle) {
        hibernateSpitterDao.saveSpittle(spittle);
    }

    @Override
    public void deleteSpittle(int id) {
        hibernateSpitterDao.deleteSpittle(id);
    }

    @Override
    public void updateSpittle(Spittle spittle) {
        hibernateSpitterDao.updateSpittle(spittle);
    }
}
