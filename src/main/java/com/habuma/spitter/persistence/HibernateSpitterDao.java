package com.habuma.spitter.persistence;

import com.habuma.spitter.domain.Spitter;
//import com.habuma.spitter.domain.Spittle;
import com.habuma.spitter.domain.Spittle;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sun.security.provider.ConfigFile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by mohit on 4/29/18.
 */
@Repository
@Transactional
public class HibernateSpitterDao implements SpitterDao {

    private SessionFactory sessionFactory;

    @Autowired
    public HibernateSpitterDao(SessionFactory sessionFactory){
        this.sessionFactory= sessionFactory;
    }

    private Session currentSession(){
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void addSpitter(Spitter spitter) {
        currentSession().save(spitter);
    }

    @Override
    public void saveSpitter(Spitter spitter) {
        currentSession().update(spitter);//<co id="co_useSession"/>
    }

    @Override
    public Spitter getSpitterById(int id) {
        return (Spitter) currentSession().get(Spitter.class, id);//<co id="co_useSession"/>
    }

    @Override
    public List<Spittle> getRecentSpittle() {
        Session session = currentSession();
        Query query = session.createQuery("FROM Spittle");
        final Collection<Spittle> retrievedSpittles = query.list();
        List<Spittle> spittlesList = new ArrayList<Spittle>(retrievedSpittles);

        //  return currentSession().f  loadAll(Spittle.class); // this isn't right...just a placeholder for now
        return spittlesList;
    }

    @Override
    public void saveSpittle(Spittle spittle) {
        currentSession().save(spittle);
    }

    @Override
    public List<Spittle> getSpittlesForSpitter(Spitter spitter) {
        Session session = currentSession();
        Query query = session.createQuery("FROM Spittle WHERE spitter_id = :spitter_id");
        query.setParameter("spitter_id", spitter.getId());
        List<Spittle> spittlesList = query.list();
        return spittlesList;
    }

    @Override
    public Spitter getSpitterByUsername(String username) {
        Session session = currentSession();
        Query query = session.createQuery("FROM Spitter WHERE username = :username");
        query.setParameter("username", username);
        Spitter spitter = (Spitter) query.uniqueResult();
        return spitter;
    }

    @Override
    public void deleteSpittle(int id) {
        currentSession().delete(getSpittleById(id));
    }

    @Override
    public Spittle getSpittleById(int id) {
        return (Spittle) currentSession().get(Spittle.class, id);
    }

    @Override
    public List<Spitter> findAllSpitters() {
        return null;
    }

    @Override
    public void updateSpittle(Spittle spittle) {
        currentSession().update(spittle);
    }

}
