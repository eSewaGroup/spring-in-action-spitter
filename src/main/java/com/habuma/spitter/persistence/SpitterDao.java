package com.habuma.spitter.persistence;

import com.habuma.spitter.domain.Spitter;
import com.habuma.spitter.domain.Spittle;
//import com.habuma.spitter.domain.Spittle;

import java.util.List;

/**
 * Created by mohit on 4/29/18.
 */

public interface SpitterDao {
    void addSpitter(Spitter spitter);

    void saveSpitter(Spitter spitter);

    Spitter getSpitterById(int id);

    List<Spittle> getRecentSpittle();

    void saveSpittle(Spittle spittle);

    List<Spittle> getSpittlesForSpitter(Spitter spitter);

    Spitter getSpitterByUsername(String username);

    void deleteSpittle(int id);

    Spittle getSpittleById(int id);

    List<Spitter> findAllSpitters();

    void updateSpittle(Spittle spittle);
}
