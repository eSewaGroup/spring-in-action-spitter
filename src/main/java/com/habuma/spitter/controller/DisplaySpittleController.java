package com.habuma.spitter.controller;

import com.habuma.spitter.service.SpitterService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;

/**
 * Created by mohit on 5/9/18.
 */
@Controller
@RequestMapping("/displaySpittle.html")
public class DisplaySpittleController {
    private final SpitterService spitterService;

    @Inject
    public DisplaySpittleController(SpitterService spitterService) {
        this.spitterService = spitterService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showSpittle(@RequestParam("id") int id, Model model ){
    model.addAttribute(spitterService.getSpittleById(id));
    return "spittles/view";
    }
}


