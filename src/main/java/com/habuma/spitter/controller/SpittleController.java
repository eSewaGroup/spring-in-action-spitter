package com.habuma.spitter.controller;

import com.habuma.spitter.domain.Spitter;
import com.habuma.spitter.domain.Spittle;
import com.habuma.spitter.service.SpitterService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sun.security.provider.ConfigFile;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.ws.RequestWrapper;
import java.util.Calendar;

/**
 * Created by mohit on 5/8/18.
 * GET = read-method,repeatable, idempotent
 * PUT= write, repeatable, idempotent
 * POST= write, non-repeatable, non-idempotent
 * DELETE= write, repeatable, idempotent
 **/
@Controller
@RequestMapping("/spittles")
public class SpittleController {
    private final SpitterService spitterService;

    @Inject
    public SpittleController(SpitterService spitterService) {
        this.spitterService = spitterService;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public String putSpittle(@PathVariable int id,
                             Model model){
        model.addAttribute("spittle",spitterService.getSpittleById(id));
       return "home";
    }

//    @RequestMapping(value = "/update", method = RequestMethod.POST)
//    public String updateSpittle(@Valid Spittle spittle){
//        System.out.println(spittle.getId());
//        Spittle s = new Spittle();
//        s.setText(spittle.getText());
//        s.setSpitter(spittle.getSpitter());
//        s.setWhen(spittle.getWhen());
//        spitterService.updateSpittle(s);
//        return "redirect:/home";
//    }

//    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
//    public String updateSpittle(@RequestParam int id,@Valid Spittle spittle){
//        spitterService.saveSpittle(spittle);
//        return "redirect:/home";
//    }

//    @RequestMapping(value = "/{id}" , method = RequestMethod.GET)
//    public @ResponseBody
//        Spittle getSpittle(@PathVariable int id){
//            return spitterService.getSpittleById(id);
//        }


    @RequestMapping(method=RequestMethod.POST, headers="Accept=text/html")
    public String createSpittleFromForm(@ModelAttribute("spittle") Spittle spittle){
//        if(result.hasErrors()) {
//            throw new BindException(result);
//        }
        String loggedUsername = SecurityContextHolder.getContext().getAuthentication().getName();

        if(spittle.getId()==0) {
            Spitter spitter = spitterService.getSpitter(loggedUsername);
            spittle.setSpitter(spitter);
            spittle.setWhen(Calendar.getInstance().getTime());
            spitterService.saveSpittle(spittle);
        }else{
            Spittle s = spitterService.getSpittleById(spittle.getId());
            s.setText(spittle.getText());
            spitterService.updateSpittle(s);
        }
        return "redirect:/";
    }

//    That status code(HttpStatus.NO_CONTENT) means that
//    the request was processed successfully, but nothing is returned in the body of the
//    response.
//    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void putSpittle(@PathVariable int id,
//                           @Valid Spittle spittle){
//        spitterService.saveSpittle(spittle);
//    }
//
//    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void deleteSpittle(@PathVariable int id){
//        spitterService.deleteSpittle(id);
//    }

    @RequestMapping(method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Spittle createSpittle(@Valid Spittle spittle,
                          BindingResult result, HttpServletResponse response)
            throws BindException {
        if(result.hasErrors()) {
            throw new BindException(result);
        }
        spitterService.saveSpittle(spittle);
        response.setHeader("Location", "/spittles/" + spittle.getId());
        return spittle;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
//    @ResponseStatus(HttpStatus.NO_CONTENT)
    public String deleteSpittleFromForm(@RequestParam int id){
        spitterService.deleteSpittle(id);
        return "redirect:/home";
    }

    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleBindException() {}
}
