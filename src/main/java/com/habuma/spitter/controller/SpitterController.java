package com.habuma.spitter.controller;

import com.habuma.spitter.domain.Spitter;
import com.habuma.spitter.service.SpitterService;
import com.habuma.spitter.util.ImageUploadException;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.security.provider.ConfigFile;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;

/**
 * Created by mohit on 5/3/18.
 */
@Controller
@RequestMapping("/spitters")
public class SpitterController {
    private final SpitterService spitterService;

    @Inject
    public SpitterController(SpitterService spitterService) {
        this.spitterService = spitterService;
    }

    @RequestMapping(value = "/spittles" , method = RequestMethod.GET)
    public String listSpittlesForSpitter(
            @RequestParam("spitter") String username, Model model ){
        Spitter spitter = spitterService.getSpitter(username);
        model.addAttribute(spitter);
        model.addAttribute(spitterService.getSpittlesForSpitter(username));
        return "spittles/list";
    }
    @RequestMapping(method = RequestMethod.GET, params ="new")
    public String createSpitterProfile(Model model){
        model.addAttribute(new Spitter());
        return "spitters/edit";
    }
    @RequestMapping(method = RequestMethod.POST)
    public String addSpitterFromForm(@Valid Spitter spitter,
                                     BindingResult bindingResult,
                                     @RequestParam(value="image", required = false)
                                     MultipartFile image,
                                     HttpSession session){
        String realPath = session.getServletContext().getRealPath("/resources");
        System.out.println(realPath);
        if(bindingResult.hasErrors()){
            return "spitters/edit";
        }
        spitterService.saveSpitter(spitter);
        try{
            if(!image.isEmpty()){
                validateImage(image);
                saveImage(spitter.getId()+".jpg", image, realPath);
            }
        } catch (ImageUploadException e){
            bindingResult.reject(e.getMessage());
            return "spitters/edit";
        }
        return "redirect:/spitters/"+spitter.getUsername();
    }

    private void saveImage(String filename, MultipartFile image, String realPath) {
        try {
            File file = new File(realPath + "/images/" + filename);
            FileUtils.writeByteArrayToFile(file, image.getBytes());
        } catch(IOException e) {
            throw new ImageUploadException("Unable to upload image", e);
        }
    }

    private void validateImage(MultipartFile image) {
        if(!image.getContentType().equals("image/jpeg")){
            throw new ImageUploadException("Only JPG images accepted");
        }
    }

    @RequestMapping(value = "/{username}",method = RequestMethod.GET)
    public String showSpitterProfile(@PathVariable String username, Model model){
        model.addAttribute(spitterService.getSpitter(username));
        return "spitters/view";
    }

//    if that handler method
//    is annotated with @ResponseBody , then it indicates that the HTTP message converter
//    mechanism should take over and transform the returned object into whatever form
//    the client needs.
//    @RequestMapping(value = "/{username}", method = RequestMethod.GET
//                   )
//    public @ResponseBody
//    Spitter getSpitter(@PathVariable String username){
//        return this.spitterService.getSpitter(username);
//    }


//    the client submits a PUT request with the data for a Spitter object
//    represented as JSON in the request’s body. To receive that message as a Spitter
//    object, we only need to annotate a handler method’s Spitter parameter with
//    @RequestBody
    @RequestMapping(value = "/{username}", method = RequestMethod.PUT )
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateSpitterUsingPUT(@PathVariable String username,
                              @RequestBody Spitter spitter) {
        spitterService.updateSpitter(spitter);
    }
}
