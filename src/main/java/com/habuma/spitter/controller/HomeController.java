package com.habuma.spitter.controller;

import com.habuma.spitter.domain.Spittle;
import com.habuma.spitter.service.SpitterService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;


/**
 * Created by mohit on 5/2/18.
 */
@Controller
public class HomeController {
    public static final int DEFAULT_SPITTLES_PER_PAGE = 25;

    private SpitterService spitterService;

    @Inject
    public HomeController(SpitterService spitterService) {
        this.spitterService = spitterService;
    }

    @RequestMapping({"/","/home"})
    public String showHomePage(Map<String, Object> model){
        model.put("spittles",spitterService.getRecentSpittles(
                DEFAULT_SPITTLES_PER_PAGE));
        model.put("spittle", new Spittle());
        return "home";
    }

}
